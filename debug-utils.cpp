#include <stdarg.h>
#include "debug-utils.hpp"
#include "debug-hook.hpp"
#include "Net-Logger.hpp"
#include <math.h>

char* fmtFloatFummy = nullptr;
char* fmtFloatHelper = nullptr;

char* fmtFloatD(float f, uint8_t precision) {
	if(precision > 4) {
		precision = 4;
	}
	if(fmtFloatHelper == nullptr) {
		fmtFloatHelper = (char*) malloc(20);
	}
	if(fmtFloatFummy != nullptr) {
		free(fmtFloatFummy);
	}
	uint32_t i1 = (uint32_t) f;
	float f2 = f - i1;
	#ifdef ARDUINO_ARCH_AVR
	f2 *= pow(10, precision);
	#else
	f2 *= pow10(precision);
	#endif
	uint32_t i2 = (uint32_t) f2;
	float f3 = f2 - i2;
	if(f3 >= 0.5) {
		i2++;
	}
	uint8_t i1l = floor( log10( abs(i1) ) ) + 1;
	fmtFloatFummy = (char*) malloc(i1l + precision + 1);
	sprintf(fmtFloatFummy, "%u.%u", i1, i2);
	return fmtFloatFummy;
}

char* fmtFloat(float f, uint8_t precision) {
	if(precision > 4) {
		precision = 4;
	}
	if(fmtFloatHelper == nullptr) {
		fmtFloatHelper = (char*) malloc(20);
	}
	uint32_t i1 = (uint32_t) f;
	float f2 = f - i1;
	#ifdef ARDUINO_ARCH_AVR
	f2 *= pow(10, precision);
	#else
	f2 *= pow10(precision);
	#endif
	uint32_t i2 = (uint32_t) f2;
	float f3 = f2 - i2;
	if(f3 >= 0.5) {
		i2++;
	}
	uint8_t i1l = floor( log10( abs(i1) ) ) + 1;
	char* str = (char*) malloc(i1l + precision + 1);
	sprintf(str, "%u.%u", i1, i2);
	return str;
}

char buf[DEBUG_UTILS_BUFF_LENGTH];
/*void _printf(const char *fmt, ... ) {
	va_list args;
	va_start(args, fmt );
	memset(buf, 0, DEBUG_UTILS_BUFF_LENGTH);
	vsnprintf(buf, DEBUG_UTILS_BUFF_LENGTH, fmt, args);
	va_end(args);
	DEBUG_UTILS_OUT.print( buf );
	DebugHook_write(buf, strlen(buf));
}
*/
void _printfLocal(const char *fmt, ... ) {
	va_list args;
	va_start(args, fmt );
	memset(buf, 0, DEBUG_UTILS_BUFF_LENGTH);
	vsnprintf(buf, DEBUG_UTILS_BUFF_LENGTH, fmt, args);
	va_end(args);
	DEBUG_UTILS_OUT.print( buf );
}
