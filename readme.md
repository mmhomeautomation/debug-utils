# debug-utils

This module is an approach to make debugging and loggin easier for systems with a serial uart, e.g. and arduino or esp based system.

## _print
Not every system has printf. This on provides one!
```c++
#include <modules/debug-utils/debug-utils.hpp>

void setup(){
	Serial.begin(921600);
}
void loop(){
	_printf("Hello world! System time in %lums", millis());
}
```
By default it uses the `Serial` object to print. You can change this in the `debug-utils.hpp`. Change the `DEBUG_UTILS_OUT` define or pass one in the platformio ini:
```ini
[env:esp07]
platform = espressif8266
board = esp07
framework = arduino
build_flags = -DDEBUG_UTILS_OUT=Serial1
```

## Logger
___

Basic usage:

`Logger *logger = new Logger(<prefix>, <max-log-level>, <prefix-color>);`

```c++
#include <modules/debug-utils/Logger.hpp>

#define MAX_LVL 3

Logger *logger = new Logger("PREFIX", MAX_LVL, C_BLUE);

void setup(){
	Serial.begin(921600);
	Logger_init(true); // true for color support
}
void loop(){
	logger->println("Hello World");
	logger->print("The answer for everyting = ");
	uint8_t answer = 42;
	logger->printlnRaw(answer);
	logger->printf("Millis: %lu ", millis());
	logger->printflnRaw("| answer with printf: %u", answer);
	logger->printfln("Answer with printf: %u", answer);
}
```

### Functions:
___
There are 2 basic methods in the logger:
- print
- printf

Both methods put the prefix of the spefic logger instance before the text:

`[<logger-name>] > <text>`

There are also different log depths. That amount of log levels can be specified when creating the logger (`MAX_LVL` in example).

- 0: `[<logger-name>] > <text>`
- 1: `[<logger-name>] > > <text>`
- 2: `[<logger-name>] > > > <text>`
- etc...

#### printf
`logger->printf(const char *fmt, ...)`

Behaves like normal printf but **without** float support.

#### printf
`logger->print(T object)`

Behaves like the normal Serial.print function and basicly the same.

#### print(f)ln

`logger->printfln(const char *fmt, ...)`

`logger->println(T object)`

They both the the same as the normal funtions **but** also print a **newline**.

#### print(f)(ln)Raw

`logger->printfRaw(const char *fmt, ...)`

`logger->printRaw(T object)`

`logger->printflnRaw(const char *fmt, ...)`

`logger->printlnRaw(T object)`

These functions just print the text without the prefix of the logger.

## Log levels

You now might ask where is the log level specified?

Every funtion described above has a brother function which does the same, but instead of log level 0 is does log level n:

`logger->printfRaw(uint8_t level, const char *fmt, ...)`

`logger->printRaw(uint8_t level, T object)`

`logger->printflnRaw(uint8_t level, const char *fmt, ...)`

`logger->printlnRaw(uint8_t level, T object)`

## Color support:

The define list for colors:

```c++
typedef enum terminalColor_e {
	C_DEFAULT,
	C_BLACK,
	C_RED,
	C_GREEN,
	C_YELLOW,
	C_BLUE,
	C_MAGENTA,
	C_CYAN,
	C_LIGHT_GRAY,
	C_DARK_GRAY,
	C_LIGHT_RED,
	C_LIGHT_GREEN,
	C_LIGHT_YELLOW,
	C_LIGHT_BLUE,
	C_LIGHT_MAGENTA,
	C_LIGHT_CYAN,
	C_WHITE
} terminalColor_t;
```

But i doesn't stop with text and background colors, also text modifications like underline, strike, bold etc.. (as long as your terminal supports it): 

```c++
typedef enum terminalMod_e {
	T_RESET,
	T_BOLD,
	T_DIM,
	T_UNDERLINE,
	T_BLINK,
	T_INVERTED,
	T_HIDDEN
} terminalMod_t;
```

### How to use colors outside of the prefix?

There are 3 macros to use for that purpose:

Change the text color:

`T_FG_COLOR(c)`

`logger->printfln("Hello %sWorld%s!", T_FG_COLOR(C_RED), T_FG_COLOR(C_DEFAULT))`

Change the background color:

`T_BG_COLOR(c)`

The modifications are a bit more complicated because the can stack.

You enable one with:

`T_MOD(mod)`

and reset one specific one with:

`T_MOD_REV(mod)`

To reset everything, use `T_MOD(T_RESET)`

## Net-Logger
___
**WIP and will not be documented at this point. Use with care!**