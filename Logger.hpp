#ifndef __LOGGER_HPP__
#define __LOGGER_HPP__

#include "debug-utils.hpp"

#define __LOGGER_NORMAL_BUFF_LENGTH 64
#define __DEFAULT_MAX_LOG_LEVEL 3

extern const char _fgcolors[][7];

extern const char _bgcolors[][8];

extern const char _termMods[][6];

extern const char _termModsReverse[][7];

extern bool serialInitialized;

typedef enum terminalColor_e {
	C_DEFAULT,
	C_BLACK,
	C_RED,
	C_GREEN,
	C_YELLOW,
	C_BLUE,
	C_MAGENTA,
	C_CYAN,
	C_LIGHT_GRAY,
	C_DARK_GRAY,
	C_LIGHT_RED,
	C_LIGHT_GREEN,
	C_LIGHT_YELLOW,
	C_LIGHT_BLUE,
	C_LIGHT_MAGENTA,
	C_LIGHT_CYAN,
	C_WHITE
} terminalColor_t;

typedef enum terminalMod_e {
	T_RESET,
	T_BOLD,
	T_DIM,
	T_UNDERLINE,
	T_BLINK,
	T_INVERTED,
	T_HIDDEN
} terminalMod_t;

#define _GET_FROM_ARRAY(i, array, arrMISize) ( i >= arrMISize ? array[0] : array[i] )

#define T_FG_COLOR(c) _GET_FROM_ARRAY(c, _fgcolors, 7)
#define T_BG_COLOR(c) _GET_FROM_ARRAY(c, _bgcolors, 8)
#define T_MOD(c) _GET_FROM_ARRAY(c, _termMods, 6)
#define T_MOD_REV(c) _GET_FROM_ARRAY(c, _termModsReverse, 7)

void Logger_init(bool colorSupport);
bool Logger_initialized(void);

#define FG_PREFIX	0
#define BG_PREFIX	1
#define FG_LOG		2
#define BG_LOG		3

class Logger {
	private:
		bool initialized = false;
		bool enabled = true;
		char** _prefix;
		char** _prefixOnly;
		char* prefix;
		uint8_t maxLevel = __DEFAULT_MAX_LOG_LEVEL;
		terminalColor_t colors[4] = {
			C_DEFAULT, C_DEFAULT, C_DEFAULT, C_DEFAULT
		};
		void init(void);

		void write(char b);
		void write(char *buf, size_t length);
		void write(char *buf){
			this->write(buf, strlen(buf));
		}
		void write(const char *buf){
			this->write((char*) buf, strlen(buf));
		}
	public:
		Logger(const char* prefix, uint8_t maxLogLevel = __DEFAULT_MAX_LOG_LEVEL, terminalColor_t fgPrefix = C_DEFAULT, terminalColor_t bgPrefix = C_DEFAULT, terminalColor_t fgLog = C_DEFAULT, terminalColor_t bgLog = C_DEFAULT);

		void setEnabled(bool enabled) {
			this->enabled = enabled;
		}

		bool isEnabled(void) {
			return this->enabled;
		}

		void printfRaw(const char *fmt, ... );

		template<typename... Args> void printf(const char *fmt, Args... args);
		template<typename... Args> void printf(uint8_t level, const char *fmt, Args... args);
		template<typename... Args> void printfln(uint8_t level, const char *fmt, Args... args);
		template<typename... Args> void printflnRaw(const char *fmt, Args... args);
		template<typename... Args> void printfln(const char *fmt, Args... args);

		void printRaw(const char* text);
		void printlnRaw(const char* text);
		void printRaw(String text);
		void printlnRaw(String text);

		void print(uint8_t level, const char* buf);
		void print(const char* buf);

		void print(uint8_t level, String text);
		void print(String text);
		
		void println(String str);
		void println(uint8_t level, String text);
		void println(const char* str);
		void println(uint8_t level, const char* text);
		
};

template<typename... Args> void Logger::printf(const char *fmt, Args... args){
	if(!this->enabled) return;
	if(!serialInitialized) return;
	if(!this->initialized && serialInitialized) this->init();
	this->write(this->_prefixOnly[0]);
	this->printfRaw(fmt, args...);
}

template<typename... Args> void Logger::printf(uint8_t level, const char *fmt, Args... args){
	if(!this->enabled) return;
	if(!serialInitialized) return;
	if(!this->initialized && serialInitialized) this->init();
	if(level >= this->maxLevel) {
		level = this->maxLevel;
	}
	this->write(this->_prefixOnly[level]);
	this->printfRaw(fmt, args...);
}

template<typename... Args> void Logger::printfln(uint8_t level, const char *fmt, Args... args){
	this->printf(level, fmt, args...);
	this->write("\n");
}

template<typename... Args> void Logger::printflnRaw(const char *fmt, Args... args){
	this->printfRaw(fmt, args...);
	this->write("\n");
}

template<typename... Args> void Logger::printfln(const char *fmt, Args... args){
	this->printf(fmt, args...);
	this->write("\n");
}

#endif