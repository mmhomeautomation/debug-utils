#ifndef __NET_LOGGER_HPP__
#define __NET_LOGGER_HPP__

#ifndef __LOGGER_PORT
#define __LOGGER_PORT 9263
#endif

#ifndef __LOGGER_MAX_CLIENTS
#define __LOGGER_MAX_CLIENTS 3
#endif

#ifdef __LOGGER_ENABLE_MQTT
#include <PubSubClient.h>
void NetLogger_MQTT_init(PubSubClient *client);

#endif

void NetLogger_TCP_init(void);
void NetLogger_handle(void);
void NetLogger_log(const char *buf);

template <typename Generic>
void NetLogger_TCP_log(Generic gen);

#endif
