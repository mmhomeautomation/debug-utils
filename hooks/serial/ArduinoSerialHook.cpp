#include "ArduinoSerialHook.hpp"
#include <Arduino.h>
#include "../../debug-hook.hpp"
#include "../../Logger.hpp"
#include <inttypes.h>
#include <stddef.h>

ArduinoHWSerialHook::ArduinoHWSerialHook(HardwareSerial* serial){
	this->serial = serial;
}

void ArduinoHWSerialHook::write(const uint8_t* buf, size_t len) {
	this->serial->write(buf, len);
}

void ArduinoHWSerialHook::write(char b) {
	this->serial->write(b);
}