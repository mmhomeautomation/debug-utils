#ifndef __HW_HOOK_ARDUINO__
#define __HW_HOOK_ARDUINO__

#include "../../debug-hook.hpp"
#include "../../Logger.hpp"
#include <inttypes.h>
#include <stddef.h>

class ArduinoHWSerialHook : public DebugHook {
	private:	
		HardwareSerial *serial = nullptr;

	public:
		ArduinoHWSerialHook(HardwareSerial *serial);
		void write(const uint8_t* buf, size_t len) override;
		void write(char b) override;
};

#endif