#if (defined(__PLATFORM_ESP32) || defined(__PLATFORM_ESP8266 ) )
#ifndef NET_LOGGER_IMPL
#define NET_LOGGER_IMPL

#include <Arduino.h>
#include <modules/linked-list/LinkedList.h>
#include <inttypes.h>
#include <stddef.h>

#include <modules/app-base/applicationState.hpp>
#include <modules/event-system/event-system.hpp>

using namespace EventSystem;

#ifdef __PLATFORM_ESP8266
	#include <ESP8266WiFi.h>
	#include <ESP8266mDNS.h>
#elif defined(__PLATFORM_ESP32)
	#include <WiFi.h>
	#include <ESPmDNS.h>
#endif

#include "NetLogger-XTensa.hpp"

NetLoggerXTensaHook::NetLoggerXTensaHook(uint16_t port, uint8_t maxClients) 
	: EventHook<ESPCoreState::ESPState_t>(EventTrigger::Type::UPDATE_MULTI) {
	this->port = port;
	this->maxClients = maxClients;
	this->_xtensaHook = new Logger("netl:xtensa", 3, C_GREEN);
}

void NetLoggerXTensaHook::init() {
	this->clients = (WiFiClient**) malloc(sizeof(WiFiClient*) * this->maxClients);
	for(uint8_t i = 0; i < this->maxClients; i++) {
		this->clients[i] = NULL;
	}
}

void NetLoggerXTensaHook::notify(ESPCoreState::ESPState_t newState, ESPCoreState::ESPState_t oldState){
	switch(newState){
		case ESPCoreState::MDNS_SETUP:{
			this->_xtensaHook->print("Creating MDNS record");
			MDNS.addService("log", "tcp", this->port);
			MDNS.addServiceTxt( "log", "tcp", "maxclients", String(this->maxClients).c_str() );
			this->_xtensaHook->printlnRaw(". OK");
			break;
		}
		case ESPCoreState::WIFI_CONNECTED:{
			this->_xtensaHook->println("Initializing TCP Logger");
			if(this->listenTCP != nullptr){ // if already setup logger was found, kill it
				this->listenTCP->end();
				delete this->listenTCP;
				this->listenTCP = nullptr;
			}
			this->listenTCP = new WiFiServer(this->port, this->maxClients);
			this->listenTCP->begin();
			for(uint8_t i = 0; i < this->maxClients; i++) {
				this->clients[i] = NULL;
			}
			if(this->listenTCP == false){
				this->_xtensaHook->println("Error creating tcp socket");
				this->listenTCP->end();
				delete this->listenTCP;
				this->listenTCP = nullptr;
			}
			this->connected = this->listenTCP;
			break;
		}
		case ESPCoreState::WIFI_CONNECTION_LOST:
		case ESPCoreState::WIFI_DISCONNECTED:{
			if(this->listenTCP != nullptr) {
				this->listenTCP->end();
				delete this->listenTCP;
				this->listenTCP = nullptr;
			}
			this->connected = false;
		}
	}
}

void NetLoggerXTensaHook::tick(void) {
	if(!this->connected) {
		return;
	}
	this->currentClient = this->listenTCP->available();
	if(this->currentClient) {
		this->_xtensaHook->println("New connection:");
		this->_xtensaHook->println( 1, currentClient.remoteIP().toString() );
		bool found = false;
		for(uint8_t i = 0; i < this->maxClients; i++) {
			if(this->clients[i] == NULL) {
				this->_xtensaHook->printfln(2, "Found slot for client: %u", i);
				this->clients[i] = new WiFiClient(this->currentClient);
				this->clients[i]->write("HELO!\n");
				found = true;
				break;
			}
		}
		if(!found) {
			this->_xtensaHook->println("No free slot");
			this->currentClient.write("No free slot\n");
			this->currentClient.stop();
		}
	}
	for(uint8_t i = 0; i < this->maxClients; i++) {
		if( this->clients[i] != NULL && !this->clients[i]->connected() ) {
			this->clients[i]->stop();
			delete this->clients[i];
			this->clients[i] = NULL;
			this->_xtensaHook->printfln("Client on slot %u disconnected", i);
		}
	}
}

void NetLoggerXTensaHook::write(const uint8_t *buf, size_t len) {
	if(!this->connected) {
		return;
	}
	for(uint8_t i = 0; i < this->maxClients; i++) {
		if(this->clients[i] != NULL) {
			this->clients[i]->write( buf, len );
		}
	}
}

void NetLoggerXTensaHook::write(char b) {
	if(!this->connected) {
		return;
	}
	for(uint8_t i = 0; i < this->maxClients; i++) {
		if(this->clients[i] != NULL) {
			this->clients[i]->write( b );
		}
	}
}

#endif
#endif