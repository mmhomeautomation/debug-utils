#if (defined(__PLATFORM_ESP32) || defined(__PLATFORM_ESP8266 ) )

#include "../../debug-hook.hpp"
#include "../../Logger.hpp"
#include <inttypes.h>
#include <stddef.h>
#include <modules/app-base/applicationState.hpp>
#include <modules/event-system/event-system.hpp>

using namespace EventSystem;

#ifdef __PLATFORM_ESP8266
	#include <ESP8266WiFi.h>
#elif defined(__PLATFORM_ESP32)
	#include <WiFi.h>
#endif

#ifndef __NETLOGGER_XTENSA__
#define __NETLOGGER_XTENSA__

class NetLoggerXTensaHook : public DebugHook, public EventHook<ESPCoreState::ESPState_t> {
	private:
		uint16_t port = 0;
		uint8_t maxClients = 0;
		bool connected = false;

		Logger *_xtensaHook = nullptr;
		WiFiClient **clients = nullptr;
		WiFiServer	*listenTCP = nullptr;
		WiFiClient currentClient;

	public:
		NetLoggerXTensaHook(uint16_t port, uint8_t maxClients);
		void write(char b) override;
		void write(const uint8_t* buf, size_t length) override;
		void init(void) override;
		void tick(void) override;
		uint16_t getPort(void) { return this->port; };
		void notify(ESPCoreState::ESPState_t newState, ESPCoreState::ESPState_t oldState) override;
};

#endif
#endif