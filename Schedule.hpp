#ifndef __SCHEDULE_HPP__
#define __SCHEDULE_HPP__

#include <Arduino.h>
#include <inttypes.h>

class Schedule
{
  private:
	uint32_t lastExecuted = 0;
	uint32_t scheduleTime = 0;
	bool enabled = true;

  public:
	Schedule(uint32_t execMS, bool enabled = true) : scheduleTime(execMS - 1), enabled(enabled)
	{
		lastExecuted = millis();
	}
	bool isEnable(void)
	{
		return this->enabled;
	}
	void enable(void)
	{
		this->enabled = true;
	}
	void disable(void)
	{
		this->enabled = false;
	}
	bool execute(void)
	{
		if (this->enabled)
		{
			if (millis() - this->lastExecuted > this->scheduleTime)
			{
				this->lastExecuted = millis();
				return true;
			}
		}
		return false;
	}
};

#endif
