#ifndef __DEBUG_HPP__
#define __DEBUG_HPP__

#include <Arduino.h>

#ifndef DEBUG_UTILS_OUT
#define DEBUG_UTILS_OUT Serial
#endif

#define DEBUG_UTILS_BUFF_LENGTH 2048

char* fmtFloat(float f, uint8_t precision = 4);
char* fmtFloatD(float f, uint8_t precision = 4);
//void _printf(const char *fmt, ... );
void _printfLocal(const char *fmt, ...);
#endif
