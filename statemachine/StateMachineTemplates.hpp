#ifndef __STATEMACHINE_TEMPLATES__
#define __STATEMACHINE_TEMPLATES__

#include "StateMachine.hpp"
#include <modules/debug-utils/Logger.hpp>
#include <modules/event-system/event-system.hpp>

template<typename T> StateMachine<T>::StateMachine(T initialState, String nameStr) : 
	EventSystem::EventSource<T>(EventSystem::EventTrigger::Type::UPDATE_MULTI, nameStr.c_str(), initialState) {}

template<typename T> void StateMachine<T>::advanceToState(T newState){
	this->previousEventParam = this->getCurrentEventParameter();
	this->currentEventParam = newState;
	if(this->decoder != nullptr){
		this->logger->printfln("Advancing from state %s to %s. Notifing %u hooks", decoder->convertToString(this->getPreviousEventParameter()), decoder->convertToString(this->getCurrentEventParameter()), this->registeredHooks->size());
	}else{
		this->logger->printfln("Advancing from state %u to %u. Notifing %u hooks", this->getPreviousEventParameter(), this->getCurrentEventParameter(), this->registeredHooks->size());
	}
	this->pushEvent(this->getCurrentEventParameter(), this->getPreviousEventParameter());
}

template<typename T> void StateMachine<T>::setStateMachineDecoder(StateMachineDecoder<T> *decoder){
	this->decoder = decoder;
}

template<typename T> T StateMachine<T>::getCurrentState(){
	return this->getCurrentEventParameter();
}

template<typename T> const char* StateMachine<T>::getCurrentStateString(){
	if(this->decoder != nullptr){
		return decoder->convertToString(this->getCurrentEventParameter());
	}else{
		this->currentStateString = String(this->getCurrentEventParameter());
		return this->currentStateString.c_str();
	}
}

#endif