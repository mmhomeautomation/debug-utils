#ifndef __STATEMACHINE_HPP__
#define __STATEMACHINE_HPP__

#include <modules/linked-list/LinkedList.h>
#include <modules/debug-utils/Logger.hpp>
#include <modules/event-system/event-system.hpp>

template<typename T> class StateMachine;

template<typename T> class StateMachineDecoder {
	public:
		virtual const char* convertToString(T state);
};

template<typename T> class StateMachine : public EventSystem::EventSource<T> {
	private:
		String currentStateString = "";
		StateMachineDecoder<T> *decoder = nullptr;

	public:
		StateMachine(T initialState, String name);
		void advanceToState(T newState);
		const char* getCurrentStateString(void);
		T getCurrentState(void);

		void setStateMachineDecoder(StateMachineDecoder<T> *decoder);
};

#include "StateMachineTemplates.hpp"

#endif