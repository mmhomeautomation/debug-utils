#if defined(__PLATFORM_ARDUINO_ETHERNET)
#ifndef NET_LOGGER_IMPL
#define NET_LOGGER_IMPL

// !!! Currently only a single client solution !!!
// TODO: Implement multi client solution

#include "../Logger.hpp"
#include "../Net-Logger.hpp"
#include <SPI.h>
#include <Ethernet.h>
#include <utility/w5100.h>

EthernetServer *listenTCP;
EthernetClient *debugClient;

Logger *netl = new Logger("netl", 2, C_GREEN);

void NetLogger_handle() {
	listenTCP->available();
	if(debugClient == nullptr) {
		for(uint8_t i = 0; i < MAX_SOCK_NUM; i++) {
			EthernetClient c(i);
			uint8_t s = c.status();
			if (s == SnSR::ESTABLISHED) {
				if(EthernetClass::_server_port[i] == __LOGGER_PORT) {
					debugClient = new EthernetClient(i);
					Serial.println("[netl] Debug client connected\n");
					break;
				}
			}
		}
	}

	if( debugClient != nullptr && !debugClient->connected() ) {
		debugClient->flush();
		debugClient->stop();
		free(debugClient);
		debugClient = nullptr;
		Serial.println("[netl] Debug client disconnected\n");
	}

}

void NetLogger_log(const char *buf) {
	if( debugClient != nullptr && debugClient->connected() ) debugClient->write( buf, strlen(buf) );
}

template <typename Generic>
void NetLogger_TCP_log(Generic gen) {
	if( debugClient != nullptr && debugClient->connected() ) debugClient->write( gen );
}

void NetLogger_TCP_init() {
	listenTCP = new EthernetServer(__LOGGER_PORT);
	listenTCP->begin();
}

#endif
#endif