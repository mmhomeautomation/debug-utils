#include "Logger.hpp"
#include "debug-utils.hpp"
#include "debug-hook.hpp"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

char dummyBuffer[256];

bool serialInitialized = false;
bool colorSupport = false;

const char _fgcolors[][7] = {
	"\e[39m\0",
	"\e[30m\0",
	"\e[31m\0",
	"\e[32m\0",
	"\e[33m\0",
	"\e[34m\0",
	"\e[35m\0",
	"\e[36m\0",
	"\e[37m\0",
	"\e[90m\0",
	"\e[91m\0",
	"\e[92m\0",
	"\e[93m\0",
	"\e[94m\0",
	"\e[95m\0",
	"\e[96m\0",
	"\e[97m\0",
};

const char _bgcolors[][8] = {
	"\e[49m\0\0",
	"\e[40m\0\0",
	"\e[41m\0\0",
	"\e[42m\0\0",
	"\e[43m\0\0",
	"\e[44m\0\0",
	"\e[45m\0\0",
	"\e[46m\0\0",
	"\e[47m\0\0",
	"\e[100m\0",
	"\e[101m\0",
	"\e[102m\0",
	"\e[103m\0",
	"\e[104m\0",
	"\e[105m\0",
	"\e[106m\0",
	"\e[107m\0",
};

const char _termMods[][6] = {
	"\e[0m\0",
	"\e[1m\0",
	"\e[2m\0",
	"\e[4m\0",
	"\e[5m\0",
	"\e[7m\0",
	"\e[8m\0",
};

const char _termModsReverse[][7] = {
	"\e[0m\0\0",
	"\e[21m\0",
	"\e[22m\0",
	"\e[24m\0",
	"\e[25m\0",
	"\e[27m\0",
	"\e[28m\0",
};

void Logger_init(bool cs) {
	serialInitialized = true;
	colorSupport = cs;
	if(cs) _printfLocal( "%s%s%slogger: init with color support\n", T_MOD(T_RESET), T_FG_COLOR(C_DEFAULT), T_BG_COLOR(C_DEFAULT) );
}

bool Logger_initialized() {
	return serialInitialized;
}

Logger::Logger(const char* prefix, uint8_t maxLogLevel, terminalColor_t fgPrefix, terminalColor_t bgPrefix, terminalColor_t fgLog, terminalColor_t bgLog) {
	this->maxLevel = maxLogLevel;
	this->prefix = (char*) prefix;
	this->colors[FG_PREFIX] = fgPrefix;
	this->colors[BG_PREFIX] = bgPrefix;
	this->colors[FG_LOG] = fgLog;
	this->colors[BG_LOG] = bgLog;
}

void Logger::init() {
	_printfLocal("logger: > Generating prefix map for logger %s\n", this->prefix);
	memset(dummyBuffer, 0, 256);
	this->_prefix = (char**) malloc(sizeof(char*) * this->maxLevel);
	this->_prefixOnly = (char**) malloc(sizeof(char*) * this->maxLevel);
	uint8_t preLength;
	if(colorSupport) {
		// <defaultColor><farbe-fg><prefix><defaultColor>:<whitespace>
		// // // <defaultColor>[<farbe-fg><prefix><defaultColor>]
		preLength = strlen(prefix) + 2;
		preLength += 2 * strlen( T_FG_COLOR(C_DEFAULT) );
		preLength += strlen( T_FG_COLOR(this->colors[FG_PREFIX]) );
	} else {
		// [<prefix>] -> 2 + prefix laenge
		preLength = strlen(prefix) + 2;
	}
	char* _generalPrefix = (char*) malloc(preLength + 1);
	memset(_generalPrefix, 0, preLength + 1);
	if(colorSupport) {
		sprintf( _generalPrefix, "%s%s%s%s: ", T_FG_COLOR(C_DEFAULT), T_FG_COLOR(this->colors[FG_PREFIX]), this->prefix, T_FG_COLOR(C_DEFAULT) );
	} else {
		sprintf(_generalPrefix, "%s: ", this->prefix);
	}
	char** __preprefix = (char**) malloc(sizeof(char**) * this->maxLevel);
	for(uint8_t i = 0; i < this->maxLevel; i++) {
		uint8_t length = (sizeof(char) * (i + 1) * 2);
		char* preprefix = (char*) malloc(length + 1);
		memset(preprefix, ' ', sizeof(char) * (i + 1) * 2);
		preprefix[length] = 0;
		for(uint8_t j = 0; j < (i + 1); j++) {
			preprefix[(j * 2)] = '>';
		}
		__preprefix[i] = preprefix;
	}
	for(uint8_t i = 0; i < this->maxLevel; i++) {
		memset(dummyBuffer, 0, 256);
		uint8_t length = sprintf(dummyBuffer, "%s%s%%s", _generalPrefix, __preprefix[i]);
		char* format = (char*) malloc(length + 1);
		memset(format, 0, length + 1);
		memcpy(format, dummyBuffer, length);

		memset(dummyBuffer, 0, 256);
		length = sprintf(dummyBuffer, "%s%s", _generalPrefix, __preprefix[i]);
		char* format2 = (char*) malloc(length + 1);
		memset(format2, 0, length + 1);
		memcpy(format2, dummyBuffer, length);
		_prefix[i] = format;
		_prefixOnly[i] = format2;
		_printfLocal("logger: > > Generated prefix     format: %s\n", format);
		_printfLocal("logger: > > Generated prefixonly format: %s\n", format2);
	}
	for(uint8_t i = 0; i < this->maxLevel; i++) {
		free(__preprefix[i]);
	}
	free(__preprefix);
	_printfLocal("logger: Prefix map generadted successfully\n");
	this->initialized = true;
}

void Logger::write(char b){
	DebugHook_write(b);
}

void Logger::write(char *buf, size_t length){
	DebugHook_write((const uint8_t*) buf, length);
}

void Logger::printfRaw(const char *fmt, ...) {
	if(!this->enabled) return;
	if(!serialInitialized) return;
	if(!this->initialized && serialInitialized) this->init();

	char loc_buf[__LOGGER_NORMAL_BUFF_LENGTH];
	char * temp = loc_buf;
	va_list arg;
	va_list copy;
	va_start(arg, fmt);
	va_copy(copy, arg);
	size_t len = vsnprintf(NULL, 0, fmt, arg);
	va_end(copy);
	if( len >= sizeof(loc_buf) ) {
		temp = new char[len + 1];
		if(temp == NULL) {
			return;
		}
	}
	len = vsnprintf(temp, len + 1, fmt, arg);
	this->write(temp, len);
	va_end(arg);
	if(len > __LOGGER_NORMAL_BUFF_LENGTH) {
		delete[] temp;
	}
}

void Logger::printRaw(const char* text){
	this->write(text);
}

void Logger::printlnRaw(const char* text){
	this->printRaw(text);
	this->write("\n");
}

void Logger::printRaw(String text){
	this->printRaw((char*) text.c_str());
}

void Logger::printlnRaw(String text){
	this->printlnRaw((char*) text.c_str());
}

void Logger::print(uint8_t level, const char* buf){
	if(!this->enabled) return;
	if(!serialInitialized) return;
	if(!this->initialized && serialInitialized) this->init();
	if(level >= this->maxLevel){
		level = this->maxLevel;
	}
	this->printRaw(_prefixOnly[level]);
	this->printRaw(buf);
}

void Logger::print(const char* buf){
	this->print(0, buf);
}

void Logger::print(uint8_t level, String text){
	this->print(0, (char*) text.c_str());
}

void Logger::print(String text){
	this->print(0, text);
}

void Logger::println(String str){
	this->print(str);
	this->write("\n");
}

void Logger::println(uint8_t level, String text){
	this->print(level, text);
	this->write("\n");
}

void Logger::println(const char* str){
	this->print(str);
	this->write("\n");
}

void Logger::println(uint8_t level, const char* text){
	this->print(level, text);
	this->write("\n");
}