#ifndef __DEBUG_HOOK__
#define __DEBUG_HOOK__

#include <inttypes.h>
#include <stddef.h>

class DebugHook {
	private:
		bool initialized = false;
	public:
		virtual void write(char b);
		virtual void write(const uint8_t* buf, size_t length);
		virtual void tick(void) {}
		virtual void init(void) {}
		
		void initHook(void){
			if(this->initialized == false){
				this->init();
				this->initialized = true;
			}
		}

		bool isInitialized(void){
			return this->initialized;
		}
};

void DebugHook_init(void);
void DebugHook_initHooks(void);
void DebugHook_register(DebugHook *hook);
void DebugHook_tick(void);

void DebugHook_write(const uint8_t* buf, size_t length);
void DebugHook_write(char b);

#endif