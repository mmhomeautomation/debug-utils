#include "debug-hook.hpp"
#include "debug-utils.hpp"
#include <modules/linked-list/LinkedList.h>
#include <inttypes.h>
#include <stdarg.h>

template<typename ... Args> void hook_deb(const char* fmt, Args ... args);

LinkedList<DebugHook*> *hooks = nullptr;

void DebugHook_register(DebugHook *hook) {
	hook_deb("Registering debug hook @%lu\n", hook);
	for(uint8_t i = 0; i < hooks->size(); i++) {
		if(hooks->get(i) == hook) {
			hook_deb("Hook already added @%u\n", i);
			return;
		}
	}
	hook_deb("Hook added\n");
	hooks->add(hook);
}

void DebugHook_init(void){
	hooks = new LinkedList<DebugHook*>();
}

void DebugHook_tick(void) {
	for(uint8_t i = 0; i < hooks->size(); i++) {
		if( hooks->get(i)->isInitialized() ) hooks->get(i)->tick();
	}
}

void DebugHook_initHooks(void) {
	for(uint8_t i = 0; i < hooks->size(); i++) hooks->get(i)->initHook();
}

void DebugHook_write(const uint8_t* buf, size_t length) {
	for(uint8_t i = 0; i < hooks->size(); i++) {
		if( hooks->get(i)->isInitialized() ) hooks->get(i)->write(buf, length);
	}
}

void DebugHook_write(char b) {
	for(uint8_t i = 0; i < hooks->size(); i++) {
		if( hooks->get(i)->isInitialized() ) hooks->get(i)->write(b);
	}
}

template<typename ... Args> void hook_deb(const char* fmt, Args ... args) {
	_printfLocal("debug:hook: > ");
	_printfLocal(fmt, args ...);
}